<?php

$subtitle = [
"It's the beginning of the month. Let's eat tacos and work hard this month!",
"We are in the middle of the month. Let's eat tacos to keep our spirits up.",
"Eat a taco and get through the last of the month!"
];

$today = new DateTime();

function getSubTitleIndex($today)
{
    if (intval($today->format('d')) < 10) {
        return 0;
    } else if(intval($today->format('d')) < 20) {
        return 1;
    }else{
        return 2;
    }
}