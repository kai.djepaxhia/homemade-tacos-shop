<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?= $title ?></title>
    <link rel="icon" href="assets/img/tacos_icon_512x512.png" type="image/x-icon"/>
    <link rel="stylesheet" href="./assets/css/main.css" type="text/css"/>
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <script src="./assets/js/darkTheme.js"></script>
</head>

<body>
<div id="title">
    <div class="container">
        <h1><?= $pageTitle ?></h1>
        <p class="sub-title"><small><?= $subtitle[getSubTitleIndex($today)] ?></small></p>
    </div>
</div>
<article>
    <section id="navigation">
        <div id="nav-bar">
            <div class="container">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index.php#about">About Us</a></li>
                        <li><a href="index.php#menu">Our Menu</a></li>
                        <li><a href="hours.php">Store Hours</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                        <li><img src="../assets/img/moon.png" class="icon"></li>
                    </ul>
                </nav>
            </div>
        </div>
        <br>
        <div class="container">
            <img
                src="assets/img/<?= $photo ?>"
                alt="<?= $photoTitle ?>"
                title="<?= $photoTitle ?>"
                width="400"
                height="267"
                loading="lazy"
            />
            <p><?= $photoTitle ?></p>
        </div>
    </section>
    <hr />