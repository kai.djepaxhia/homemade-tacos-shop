<?php

include 'includes/subTitles.php';

$title = 'Tacos Shop';
$pageTitle = 'Welcome to Homemade Tacos Shop!';
$photo = "tacos_and_drink_400x267.png";
$photoTitle = 'Tacos and a drink';

?>
<?php include 'includes/header.php'; ?>

    <section id="about">
        <div class="container">
            <h2>About <abbr title="Homemade Tacos Shop">HTS</abbr></h2>
            <p>
                <abbr title="Homemade Tacos Shop">HTS</abbr> was founded in 2023. Our
                shop was build from a <strong>love of tacos</strong>. <br/>
                Our shop adds a unique and interesting place to our little town!
            </p>
            <h3>Taco Quiz:</h3>
            <aside>
                <details>
                    <summary>When did tacos first appear in France?</summary>
                    <p>It was in the late 2000's!</p>
                </details>
            </aside>
        </div>
    </section>
    <hr/>
    <section id="menu">
        <div class="container">
            <h2>Our Menu</h2>
            <table>
                <caption>
                    Our Tacos
                </caption>
                <thead>
                <tr>
                    <th>&nbsp;Tacos</th>
                    <th>Qty</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="3">Crunchy</th>
                    <td>1</td>
                    <td>$1.50</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>$2.50</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>$3.25</td>
                </tr>
                <tr>
                    <th rowspan="3">Soft</th>
                    <td>1</td>
                    <td>$2.00</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>$3.50</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>$4.50</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">Chips & Salsa $2</td>
                </tr>
                </tfoot>
            </table>
        </div>
    </section>
    <br/>
    <a href="/" class="back-to-top button">Back to the Top</a>
</article>
</body>
<footer>
    <div id="copyright">
        <div class="container">
            <p>Copyright &copy; Homemade Tacos Shop</p>
        </div>
    </div>
</footer>
</html>
