<?php

include 'includes/subTitles.php';

$title = 'Store Hours';
$pageTitle = 'Homemade Tacos Shop Hours';
$photo = "tacos_tray_400x267.png";
$photoTitle = 'A Tray of Tasty Tacos';

?>
<?php include 'includes/header.php'; ?>

      <section>
        <div class="container">
          <h2>We are open 7 days a week!</h2>
          <p>
            Sunday-Thursday : <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11am-9pm
            <br />Friday-Saturday : <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11am-11pm
          </p>
        </div>
      </section>
      <br />
      <a href="hours.php" class="back-to-top button">Back to the Top</a>
      <p></p>
    </article>
  </body>
  <footer>
    <div id="copyright">
      <div class="container">
        <p>Copyright &copy; Homemade Tacos Shop</p>
      </div>
    </div>
  </footer>
</html>
