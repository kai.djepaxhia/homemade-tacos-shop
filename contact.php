<?php

include 'includes/subTitles.php';

$title = 'Store Contact';
$pageTitle = 'Homemade Tacos Shop Contact';
$photo = 'tacos_close_up_400x260.png';
$photoTitle = 'Homemade Tacos Shop Tacos';

?>
<?php include 'includes/header.php'; ?>
      <section>
        <div class="container">
          <h2>Our Contact Form</h2>
          <form class="form" action="https://httpbin.org/get" method="get">
            <fieldset>
              <legend>Send us a message</legend>
              <div class="form1">
                <label for="name">Name :</label>
                <input
                  class="form-control"
                  type="text"
                  name="name"
                  id="name"
                  placeholder="Your Name"
                  autocomplete="on"
                  required
                /><br /><br />
              </div>
              <div class="form1">
                <label for="email">Email :</label>
                <input
                  class="form-control"
                  type="text"
                  name="email"
                  id="email"
                  placeholder="Your Email"
                  autocomplete="on"
                  required
                /><br /><br />
              </div>
              <div class="form1">
                <label for="message">Message :</label><br />
                <textarea
                  class="form-control"
                  name="message"
                  id="message"
                  cols="100"
                  rows="10"
                  placeholder="Type your message here"
                  required
                ></textarea>
              </div>
            </fieldset>
            <br />
            &nbsp;<input class="button1" type="submit" value="Submit" />
            <input class="button1" type="reset" value="Reset" />
          </form>
          <br />
        </div>
      </section>
      <hr />
      <section>
        <div class="container">
          <h2>Our Location</h2>
          <address>
            727 Rue du Tacos <br />
            75200 Paris
          </address>
          <p>
            Phone: <a href="tel:+0611223344" class="button1">06 11 22 33 44</a>
          </p>
        </div>
      </section>
      <a href="contact.php" class="back-to-top button">Back to the Top</a>
    </article>
  </body>
  <footer>
    <div id="copyright">
      <div class="container">
        <p>Copyright &copy; Homemade Tacos Shop</p>
      </div>
    </div>
  </footer>
</html>
