window.onload = function() {
    const icons = document.getElementsByClassName('icon');

    [...icons].forEach(element => {
    element.addEventListener('click', function () {
            document.body.classList.toggle("dark-theme")
            if (document.body.classList.contains("dark-theme")) {
                element.src = "assets/img/sun.png"
            } else {
                element.src = "assets/img/moon.png"
            }
        })
    });
};

